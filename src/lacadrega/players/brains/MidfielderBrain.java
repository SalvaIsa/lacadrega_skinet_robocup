package lacadrega.players.brains;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Queue;

import lacadrega.CBMessage;
import lacadrega.planning.DefenderAgent;
import lacadrega.planning.MidfielderAgent;
import lacadrega.planning.goals.MidfielderGoal;
import lacadrega.players.MidfielderPlayer;
import lg.planner.GoapAction;
import skinet.framework.Player;
import skinet.framework.Pos;
import skinet.framework.objInfos.ObjBall;
import skinet.framework.objInfos.ObjInfo;
import skinet.framework.objInfos.ObjPlayer;
import skinet.framework.objInfos.TeamUnum;

public class MidfielderBrain extends Thread {

	public static final double MIDFIELDER_CLOSER_AREA = 6;
	public static double MIDFIELDER_FARER_AREA = 11.0;
	public static final double MIDFIELDER_INFRONTOF_DIRECTION = 10;
	public static final double OPPAREA_CLOSE_ENOUGH_X = 30;
	public static final double OPPAREA_TOP_Y = 20;
	public static final double OPPAREA_BOTTOM_Y = -20;
	private static final double MIDFIELDER_DIST_TO_BALL_X = 20;

	private MidfielderPlayer player;
	public MidfielderAgent agent;

	private Queue<GoapAction> currentActionGoal;
	private boolean isOurHome;

	public ObjPlayer playerToMark = null;
	public Pos positionToMark = null;

	private int turning_steps = 1;
	private boolean turning_checking = false;

	public MidfielderBrain() {
		super();
	}

	public MidfielderBrain(MidfielderPlayer p) {

		this.player = p;
		start();
	}

	@Override
	public void run() {
		super.run();

		while (true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			if (player.getMem().timeCheck(player.getTime())) {
				player.setTime(player.getMem().ObjMem.getTime());

				//				if (player.getMem().side.compareTo("l") == 0) {

				if( player.getMem().playMode.contains("kick_off") 
						|| player.getMem().playMode.contains("goal")
						|| player.getMem().playMode.contains("free_kick") 
						|| player.getMem().playMode.contains("offside") )
				{						
					
					player.getMem().cbMessageReceived = null;

					player.getMem().currHome.x = player.getMem().home.x;
					player.getMem().currHome.y = player.getMem().home.y;

					player.getAction().goInitHome();

				}else if( player.getMem().playMode.contains("play_on"))
				{
					agent = new MidfielderAgent();

					agent.initWorldState();
					checkCurrentHome();
					checkAgentWorldState();

					//						System.out.println("CURR " + player.getMem().currHome.x + "   " +
					//								player.getMem().currHome.y);
					//						System.out.println("HOME " + player.getMem().home.x + "   " +
					//								player.getMem().home.y);	

					playerComunication_CheckClosest();

					agent.setGoalPlanner(new MidfielderGoal());

					agent.update();

					if (this.agent.getGoalActions() == null) {

						if (player.getMem().isHome || isOurHome)
							try {
								isOurHome = true;
								if (!agent.getWorldStateProperty(MidfielderAgent.SEE_THE_BALL))
									player.getRoboClient().turn(Player.TURNING_DIRECTION);
							} catch (UnknownHostException e) {
								e.printStackTrace();
							}
						else {

							if (!turning_checking) {

								player.getAction().goCurrentHome();

								turning_steps++;
								if (turning_steps >= Player.MIDFIELDER_TURNING_TIMES)
									turning_checking = true;
							}

							if (turning_checking) {
								try {

									if (!agent.getWorldStateProperty(MidfielderAgent.SEE_THE_BALL))
										player.getRoboClient().turn(Player.TURNING_DIRECTION);
									else
										turning_steps = 0;

									turning_steps--;

									if (turning_steps <= 0)
										turning_checking = false;

								} catch (UnknownHostException e) {
									e.printStackTrace();
								}
							}

						}
					} else {

						isOurHome = false;

						currentActionGoal = this.agent.getGoalActions();
						GoapAction actionToDo = currentActionGoal.peek();
						if (actionToDo.isDone()) {
							currentActionGoal.poll();
							if (currentActionGoal.isEmpty())
								currentActionGoal = null;
						} else
							actionToDo.perform(player);
					}

				}
				//				}
			}

		}
	}


	private void checkCurrentHome() {

		if(player.getMem().currHome == null)
		{
			player.getMem().currHome = new Pos();
			player.getMem().currHome.x = player.getMem().home.x;
			player.getMem().currHome.y = player.getMem().home.y;

			//			player.getMem().currHome = player.getMem().home;
		}

		CBMessage cbMess = player.getMem().cbMessageReceived;

		if(cbMess != null)
		{
			int diff = (int) (cbMess.ballX - player.getMem().currHome.x);
			switch(cbMess.uNum)
			{
			case 2:
			case 3:
			case 4: 
				if(Math.abs(diff) != Player.DISTANCE_DEF_MID)
					player.getMem().currHome.x = cbMess.ballX -Player.DISTANCE_DEF_MID;
				break;

			case 5:
			case 6:
			case 7:
			case 8:
				player.getMem().currHome.x = cbMess.ballX;
				break;
			case 9:
			case 10:
			case 11:
				if(Math.abs(diff) != Player.DISTANCE_MID_STRK)
					player.getMem().currHome.x= cbMess.ballX +Player.DISTANCE_MID_STRK;
				break;

			}

		}
		else
		{
			ObjBall ball = player.getMem().getBall();

			if(ball != null)
			{
				if(player.getMem().getBallPos(ball).x <= 0)
				{
					player.getMem().currHome.x = player.getMem().home.x;
				}
				else
				{
					double x_toAssign = player.getMem().getBallPos(ball).x - MidfielderBrain.MIDFIELDER_DIST_TO_BALL_X;
					player.getMem().currHome.x = x_toAssign;
				}
			}

		}


		isOurHome = false;
	}

	private void checkAgentWorldState() {

		if (player.getMem().isObjVisible("ball")) {

			agent.setWorldStateProperty(MidfielderAgent.SEE_THE_BALL, true);

			double distToBall = ((ObjInfo) player.getMem().getBall()).getDistance();
			Pos ballPos = player.getMem().getBallPos(player.getMem().getBall());

			if (ballPos.x >= OPPAREA_CLOSE_ENOUGH_X)
			{
				agent.setWorldStateProperty(MidfielderAgent.BALL_IN_OPPONENT_CROSS_AREA, true);

				if (ballPos.y <= OPPAREA_TOP_Y && ballPos.y >= OPPAREA_BOTTOM_Y)
					agent.setWorldStateProperty(MidfielderAgent.BALL_IN_OPPONENT_AREA, true);
			}

			//			if (ballPos.x <= DefenderPlayer.MAX_DEFENDER_X)
			//				agent.setWorldStateProperty(MidfielderAgent.BALL_IN_DEFENSE, true);

			if (distToBall <= DefenderBrain.BALL_POSSESSION_RANGE)
				agent.setWorldStateProperty(MidfielderAgent.HAVE_THE_BALL, true);
			else {
				TeamUnum closestPlayerToBall = player.getMem().getClosestPlayerToBall(player.getRoboClient().getTeam(), false);

				if ((player.getMem().uNum == closestPlayerToBall.uNum
						&& player.getRoboClient().getTeam().equals(closestPlayerToBall.team)))
					//						|| (closestPlayerToBall.uNum >= 9 && closestPlayerToBall.uNum <= 11
					//								&& closestPlayerToBall.distance > StrikerBrain.STRIKER_CLOSEST_RANGE
					//										+ MidfielderBrain.STRIKER_DELTA_RANGE))
					agent.setWorldStateProperty(MidfielderAgent.CLOSEST_TO_THE_BALL, true);
				else {
					if (player.getRoboClient().getTeam().equals(closestPlayerToBall.team)) {
						if (closestPlayerToBall.distance <= DefenderBrain.BALL_POSSESSION_RANGE)
							agent.setWorldStateProperty(MidfielderAgent.BALL_TO_FRIENDS, true);

						agent.setWorldStateProperty(MidfielderAgent.FRIEND_CLOSEST_TO_BALL, true);
					} else {
						if (closestPlayerToBall.distance <= DefenderBrain.BALL_POSSESSION_RANGE)
							agent.setWorldStateProperty(MidfielderAgent.BALL_TO_OPPONENT, true);

						agent.setWorldStateProperty(MidfielderAgent.OPPONENT_CLOSEST_TO_BALL, true);
					}

				}
			}
		}
		// Check avversari davanti vicini o lontani

		worldState_checkFarCloseThings();

		/*
		 * 
		 */

		ArrayList<ObjPlayer> seenPlayers2 = player.getMem().getPlayers();

		for (ObjPlayer c : seenPlayers2) {
			boolean freeForPassage = true;

			if (c.getTeam() != null) {
				if (c.getTeam().equals(player.getRoboClient().getTeam())) {
					for (ObjPlayer opp : seenPlayers2) {
						if (opp.getTeam() != null) {
							if (!opp.getTeam().equals(player.getRoboClient().getTeam())) {
								if (Math.abs(c.getDirection())
										- Math.abs(opp.getDirection()) <= MIDFIELDER_INFRONTOF_DIRECTION
										&& c.getDistance() > opp.getDistance()) {

									agent.setWorldStateProperty(MidfielderAgent.OPPONENT_IN_FRONT_FRIEND, true);
									freeForPassage = false;

									break;
								}
							}
						}
					}

					if (freeForPassage) {
						agent.setWorldStateProperty(MidfielderAgent.FREE_FRIEND_FOR_PASSAGE, true);

						if (c.getuNum() == 9 || c.getuNum() == 10 || c.getuNum() == 11) {
							agent.setWorldStateProperty(MidfielderAgent.FREE_STRIKER_FOR_PASSAGE, true);
						}
					}

					if (agent.getWorldStateProperty(MidfielderAgent.OPPONENT_IN_FRONT_FRIEND))
						break;
				}
			}
		}

	}

	private void worldState_checkFarCloseThings() {

		ArrayList<ObjPlayer> seenPlayers = player.getMem().getPlayers();

		double minOpponentDist = 104.0;
		ObjPlayer minOppoPlayer = null;

		if (seenPlayers.size() != 0) {
			for (ObjPlayer p : seenPlayers) {
				if (p.getTeam() != null) {
					if (Math.abs(p.getDirection()) - Math.abs(player.getDirection()) <= MIDFIELDER_INFRONTOF_DIRECTION)
						if (p.getTeam().equals(player.getRoboClient().getTeam())) {
							if (p.getDistance() <= MIDFIELDER_CLOSER_AREA) {
								agent.setWorldStateProperty(DefenderAgent.SEE_CLOSE_FRIEND, true);
							} else {
								agent.setWorldStateProperty(DefenderAgent.SEE_FAR_FRIEND, true);
							}

						} else {

							if(p.getDistance() < minOpponentDist)
							{
								minOpponentDist = p.getDistance();
								minOppoPlayer = p;
							}

							if (p.getDistance() <= MIDFIELDER_CLOSER_AREA) {
								agent.setWorldStateProperty(DefenderAgent.SEE_CLOSE_OPPONENT, true);

							}

							if (p.getDistance() <= MIDFIELDER_FARER_AREA) {
								agent.setWorldStateProperty(DefenderAgent.SEE_FAR_OPPONENT, true);
							}
						}
				}
			}
		}

		if(minOppoPlayer != null)
		{
			if(playerToMark == null || minOppoPlayer.getuNum() == playerToMark.getuNum())
			{
				playerToMark = minOppoPlayer;
				positionToMark =  player.getMem().getOpponentPos(playerToMark);
			}
			else if(playerToMark != null && minOppoPlayer.getuNum() != playerToMark.getuNum())
			{
				playerToMark = null;
				positionToMark =  null;				
			}
		}
		else
		{
			playerToMark = null;
			positionToMark =  null;
		}
	}

	//	private void playerComunication_CheckClosest() {
	//
	//		if(agent.getWorldStateProperty(MidfielderAgent.CLOSEST_TO_THE_BALL))
	//		{
	//			Pos ballPos = player.getMem().getBallPos(player.getMem().getBall());
	//
	//			if(player.getMem().currHome != null)
	//				player.getMem().currHome.x = (int)ballPos.x;
	//
	//			player.messageClosest = "CBw" + player.getMem().uNum + "w" + (int)ballPos.x + "w" + (int)ballPos.y;
	//			player.ableSayingClosest = true;
	//		}
	//		else
	//			player.ableSayingClosest = false;
	//	}

	/*
	 * CBMessage -> CB w uNum w ball.x w ball.y w ball.distance
	 */
	private void playerComunication_CheckClosest() {

		player.messageClosest = "void";

		CBMessage cbMess = player.getMem().cbMessageReceived;

		if(agent.getWorldStateProperty(DefenderAgent.CLOSEST_TO_THE_BALL))
		{
			if(cbMess == null 
					|| cbMess.ballDistance > player.getMem().getBall().getDistance() 
					|| cbMess.uNum == player.getMem().uNum)
			{
				Pos ballPos = player.getMem().getBallPos(player.getMem().getBall());

				if(player.getMem().currHome != null)
					player.getMem().currHome.x = (int)ballPos.x;

				player.messageClosest = "CBw" + player.getMem().uNum + "w" + (int)ballPos.x + "w" + (int)ballPos.y
						+ "w" + (int)player.getMem().getBall().getDistance();

			}
			else
			{
				//				agent.setWorldStateProperty(DefenderAgent.CLOSEST_TO_THE_BALL, false);

				player.messageClosest = "CBw" + cbMess.uNum + "w" + cbMess.ballX + "w" + cbMess.ballY
						+ "w" + cbMess.ballDistance;
			}
		}
		else
		{
			if(cbMess != null)
				player.messageClosest = "CBw" + cbMess.uNum + "w" + cbMess.ballX + "w" + cbMess.ballY
				+ "w" + cbMess.ballDistance;		
		}

		if(player.messageClosest.equals("void"))
		{
			player.ableSayingClosest = false;
		}
		else
			player.ableSayingClosest = true;
	}

}
