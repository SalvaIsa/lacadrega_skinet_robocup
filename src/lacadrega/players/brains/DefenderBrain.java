package lacadrega.players.brains;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Queue;

import lacadrega.CBMessage;
import lacadrega.planning.DefenderAgent;
import lacadrega.planning.goals.DefenderGoal;
import lacadrega.players.DefenderPlayer;
import lg.planner.GoapAction;
import skinet.framework.Player;
import skinet.framework.Polar;
import skinet.framework.Pos;
import skinet.framework.objInfos.ObjBall;
import skinet.framework.objInfos.ObjGoal;
import skinet.framework.objInfos.ObjInfo;
import skinet.framework.objInfos.ObjPlayer;
import skinet.framework.objInfos.TeamUnum;

public class DefenderBrain extends Thread {

	public static final double TO_MARK_RANGE = 17.0;
	public static final double BALL_POSSESSION_RANGE = 0.7;
	private static double DEFENDER_CLOSER_AREA = 8.0;
	private static double DEFENDER_FARER_AREA = 22.0;
	private static double DEFENDER_DIRECTION_INFRONTOF = 7.0;

	public static double MAX_DEFENDER_X = -10.0;
	public static double MIN_DEFENDER_X = -45.0;

	public static double DEFENDER_DIST_TO_BALL_X = 40;

	private DefenderPlayer player;
	public DefenderAgent agent;

	public ObjPlayer playerToMark = null;
	public Pos positionToMark = null;

	private Queue<GoapAction> currentActionGoal;
	private boolean isOurHome = false;

	private int turning_steps = 1;
	private boolean turning_checking = false;

	public DefenderBrain() {
		super();
	}

	public DefenderBrain(DefenderPlayer p) {

		this.player = p;

		start();
	}

	/*
	 * Chi va verso la palla lo comunica agli altri, così gli altri non vanno
	 * pure
	 * 
	 * Chi prende la palla lo comunica agli altri, così fanno le cose successive
	 */

	@Override
	public void run() {

		while (true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			if (player.getMem().timeCheck(player.getTime())) {
				player.setTime(player.getMem().ObjMem.getTime());

				//				if (player.getMem().side.compareTo("l") == 0) {

				if( player.getMem().playMode.contains("kick_off") 
						|| player.getMem().playMode.contains("goal")
						|| player.getMem().playMode.contains("free_kick") 
						|| player.getMem().playMode.contains("offside") )
				{					
					player.getMem().cbMessageReceived = null;
					
					player.getMem().currHome.x = player.getMem().home.x;
					player.getMem().currHome.y = player.getMem().home.y;

					player.getAction().goInitHome();					
				}
				else if( player.getMem().playMode.contains("play_on"))
				{
					checkCurrentHome();

					agent = new DefenderAgent();

					agent.initWorldState();

					checkAgentWorldState();

					playerComunication_CheckClosest();

					agent.setGoalPlanner(new DefenderGoal());

					ObjBall b = player.getMem().getBall();

					if(player.getMem().getPosition().x <= -27 && b!= null && b.getDistance() <= DefenderBrain.BALL_POSSESSION_RANGE)
					{
						player.getAction().kickToGoal(b);
					}
					else
					{
						this.agent.update();

						if (this.agent.getGoalActions() == null) {

							if (player.getMem().isHome || isOurHome)
								try {
									if (!agent.getWorldStateProperty(DefenderAgent.SEE_THE_BALL))
									{
										player.getRoboClient().turn(Player.TURNING_DIRECTION);
									}
									isOurHome = true;
								} catch (UnknownHostException e) {
									e.printStackTrace();
								}
							else {

								if(!turning_checking)
								{
									player.getAction().goCurrentHome();

									turning_steps++;
									if(turning_steps >= Player.DEFENDER_TURNING_TIMES)
										turning_checking = true;
								}

								if ( turning_checking )
								{
									try {

										if(!agent.getWorldStateProperty(DefenderAgent.SEE_THE_BALL))
											player.getRoboClient().turn(Player.TURNING_DIRECTION);
										else
											turning_steps = 0;

										turning_steps--;

										if(turning_steps <= 0)
											turning_checking = false;

									} catch (UnknownHostException e) {
										e.printStackTrace();
									}
								}


							}
						} else{

							isOurHome = false;

							currentActionGoal = this.agent.getGoalActions();

							GoapAction actionToDo = currentActionGoal.peek();
							if (actionToDo.isDone()) {
								currentActionGoal.poll();
								if (currentActionGoal.isEmpty())
									currentActionGoal = null;
							} else
								actionToDo.perform(player);
						}

					}
				}
				//				}
			}

		}

	}

	private void checkCurrentHome() {

		if(player.getMem().currHome == null)
		{
			player.getMem().currHome = new Pos();
			player.getMem().currHome.x = player.getMem().home.x;
			player.getMem().currHome.y = player.getMem().home.y;

			//			player.getMem().currHome = player.getMem().home;
		}

		CBMessage cbMess = player.getMem().cbMessageReceived;
		double x_toAssign = 0.0;

		if(cbMess != null)
		{
			int diff = (int) (cbMess.ballX - player.getMem().currHome.x);

			switch(cbMess.uNum)
			{
			case 2:
			case 3:
			case 4: 

				if(cbMess.ballX >= DefenderBrain.MAX_DEFENDER_X)
					x_toAssign = DefenderBrain.MAX_DEFENDER_X;
				else if (cbMess.ballX <= DefenderBrain.MIN_DEFENDER_X)
					x_toAssign = DefenderBrain.MIN_DEFENDER_X;
				else
					x_toAssign = cbMess.ballX;

				player.getMem().currHome.x = x_toAssign;
				break;

			case 5:
			case 6:
			case 7:
			case 8:

				if(Math.abs(diff) != Player.DISTANCE_DEF_MID)
				{					
					int maybeX = cbMess.ballX -Player.DISTANCE_DEF_MID;

					if(maybeX >= DefenderBrain.MAX_DEFENDER_X)
						x_toAssign = DefenderBrain.MAX_DEFENDER_X;
					else if (maybeX <= DefenderBrain.MIN_DEFENDER_X)
						x_toAssign = DefenderBrain.MIN_DEFENDER_X;
					else
						x_toAssign = maybeX;

					player.getMem().currHome.x = x_toAssign;
				}
				break;
			case 9:
			case 10:
			case 11:
				if(Math.abs(diff) != Player.DISTANCE_DEF_STRK)
				{
					int maybeX = cbMess.ballX - Player.DISTANCE_DEF_STRK;

					if(maybeX >= DefenderBrain.MAX_DEFENDER_X)
						x_toAssign = DefenderBrain.MAX_DEFENDER_X;
					else if (maybeX <= DefenderBrain.MIN_DEFENDER_X)
						x_toAssign = DefenderBrain.MIN_DEFENDER_X;
					else
						x_toAssign = maybeX;

					player.getMem().currHome.x = x_toAssign;
				}
				break;

			}

		}
		else
		{
			ObjBall ball = player.getMem().getBall();

			if(ball != null)
			{
				if(player.getMem().getBallPos(ball).x <= 0)
				{
					player.getMem().currHome.x = player.getMem().home.x;
				}
				else
				{
					double x_toAssign2 = player.getMem().getBallPos(ball).x - DefenderBrain.DEFENDER_DIST_TO_BALL_X;

					if(x_toAssign2 >= DefenderBrain.MAX_DEFENDER_X)
						x_toAssign2 = DefenderBrain.MAX_DEFENDER_X;
					else if(x_toAssign2 <= DefenderBrain.MIN_DEFENDER_X)
						x_toAssign2 = DefenderBrain.MIN_DEFENDER_X;

					player.getMem().currHome.x = x_toAssign2;
				}
			}
		}

		isOurHome = false;
	}

	private void checkAgentWorldState() {

		//		if (player.getPosition().x <= DefenderPlayer.MAX_DEFENDER_X)
		//			agent.setWorldStateProperty(DefenderAgent.PLAYER_IN_DEFENSE, true);


		if (player.getMem().isObjVisible("ball")) {

			agent.setWorldStateProperty(DefenderAgent.SEE_THE_BALL, true);

			Pos ballPos = player.getMem().getBallPos(player.getMem().getBall());
			double distToBall = ((ObjInfo) player.getMem().getBall()).getDistance();

			if (ballPos.x <= DefenderBrain.MAX_DEFENDER_X)
				agent.setWorldStateProperty(DefenderAgent.BALL_IN_DEFENSE, true);

			if (distToBall <= BALL_POSSESSION_RANGE)
				agent.setWorldStateProperty(DefenderAgent.HAVE_THE_BALL, true);
			else {
				TeamUnum closestPlayerToBall = player.getMem().getClosestPlayerToBall(player.getRoboClient().getTeam(), false);

				if (player.getMem().uNum == closestPlayerToBall.uNum
						&& player.getRoboClient().getTeam().equals(closestPlayerToBall.team))
					agent.setWorldStateProperty(DefenderAgent.CLOSEST_TO_THE_BALL, true);
				else {
					if (player.getRoboClient().getTeam().equals(closestPlayerToBall.team)) {
						if (closestPlayerToBall.distance <= BALL_POSSESSION_RANGE)
							agent.setWorldStateProperty(DefenderAgent.BALL_TO_FRIENDS, true);

						agent.setWorldStateProperty(DefenderAgent.FRIEND_CLOSEST_TO_BALL, true);
					} else {
						if (closestPlayerToBall.distance <= BALL_POSSESSION_RANGE)
							agent.setWorldStateProperty(DefenderAgent.BALL_TO_OPPONENT, true);

						agent.setWorldStateProperty(DefenderAgent.OPPONENT_CLOSEST_TO_BALL, true);
					}

				}
			}
		}

		/*
		 * Inviamo messaggi agli altri giocatori e controlliamo quello che
		 * sentiamo noi, così da settare le proprietà del WorldState anche senza
		 * vedere la palla
		 * 
		 * [es: pallaAll'Avversario, pallaAiCompagni, etc...]
		 */

		/*
		 * Check vedeAvversario, vedeCompagno comunista
		 */

		worldState_SeeFarCloseThings();

		/*
		 * CHECK avversarioDavantiCompagno, compagnoFreePerPassaggio
		 */

		ArrayList<ObjPlayer> seenPlayers2 = player.getMem().getPlayers();

		for (ObjPlayer c : seenPlayers2) {
			boolean freeForPassage = true;

			if (c.getTeam() != null) {
				if (c.getTeam().equals(player.getRoboClient().getTeam())) {
					for (ObjPlayer opp : seenPlayers2) {
						if (opp.getTeam() != null) {
							if (!opp.getTeam().equals(player.getRoboClient().getTeam())) {
								if (Math.abs(c.getDirection())
										- Math.abs(opp.getDirection()) <= DEFENDER_DIRECTION_INFRONTOF
										&& c.getDistance() > opp.getDistance()) {

									agent.setWorldStateProperty(DefenderAgent.OPPONENT_IN_FRONT_FRIEND, true);
									freeForPassage = false;

									break;
								}
							}
						}
					}

					if (freeForPassage)
						agent.setWorldStateProperty(DefenderAgent.FREE_FRIEND_FOR_PASSAGE, true);

					if (agent.getWorldStateProperty(DefenderAgent.OPPONENT_IN_FRONT_FRIEND))
						break;
				}

			}
		}

	}

	private void worldState_SeeFarCloseThings() {

		ArrayList<ObjPlayer> seenPlayers = player.getMem().getPlayers();

		double minOpponentDist = 104.0;
		ObjPlayer minOppoPlayer = null;

		if (seenPlayers.size() != 0) {
			for (ObjPlayer p : seenPlayers) {
				if (p.getTeam() != null) {
					if (p.getTeam().equals(player.getRoboClient().getTeam())) {
						if (p.getDistance() <= DEFENDER_CLOSER_AREA) {
							agent.setWorldStateProperty(DefenderAgent.SEE_CLOSE_FRIEND, true);
						} else {
							agent.setWorldStateProperty(DefenderAgent.SEE_FAR_FRIEND, true);
						}
					} else {

						if(p.getDistance() < minOpponentDist)
						{
							minOpponentDist = p.getDistance();
							minOppoPlayer = p;
						}	

						if (p.getDistance() <= DEFENDER_CLOSER_AREA) {
							agent.setWorldStateProperty(DefenderAgent.SEE_CLOSE_OPPONENT, true);		

						} 

						if(p.getDistance() <= DEFENDER_FARER_AREA){
							agent.setWorldStateProperty(DefenderAgent.SEE_FAR_OPPONENT, true);
						}
					}
				}
			}
		}

		if(minOppoPlayer != null)
		{
			if(playerToMark == null || minOppoPlayer.getuNum() == playerToMark.getuNum())
			{
				playerToMark = minOppoPlayer;
				positionToMark =  player.getMem().getOpponentPos(playerToMark);

				if(positionToMark.x <= DefenderBrain.MAX_DEFENDER_X)
				{
					agent.setWorldStateProperty(DefenderAgent.TO_MARK_OPP_IN_DEF_MAX_AREA, true);

					if(positionToMark.x <= player.getMem().currHome.x + TO_MARK_RANGE
							&& positionToMark.x >= player.getMem().currHome.x - TO_MARK_RANGE)
					{
						agent.setWorldStateProperty(DefenderAgent.TO_MARK_OPP_IN_HOME, true);
					}
					else
					{
						playerToMark = null;
						positionToMark =  null;				
					}

				}
				else
				{
					playerToMark = null;
					positionToMark =  null;				
				}		
			}
			else if(playerToMark != null && minOppoPlayer.getuNum() != playerToMark.getuNum())
			{
				playerToMark = null;
				positionToMark =  null;				
			}
		}
		else
		{
			playerToMark = null;
			positionToMark =  null;
		}
	}

	/*
	 * CBMessage -> CB w uNum w ball.x w ball.y w ball.distance
	 */
	private void playerComunication_CheckClosest() {

		player.messageClosest = "void";

		CBMessage cbMess = player.getMem().cbMessageReceived;

		if(agent.getWorldStateProperty(DefenderAgent.CLOSEST_TO_THE_BALL))
		{
			if(cbMess == null 
					|| cbMess.ballDistance > player.getMem().getBall().getDistance() 
					|| cbMess.uNum == player.getMem().uNum)
			{
				Pos ballPos = player.getMem().getBallPos(player.getMem().getBall());

				if(player.getMem().currHome != null && cbMess != null)
				{
					double x_toAssign;

					if(cbMess.ballX >= DefenderBrain.MAX_DEFENDER_X)
						x_toAssign = DefenderBrain.MAX_DEFENDER_X;
					else if (cbMess.ballX <= DefenderBrain.MIN_DEFENDER_X)
						x_toAssign = DefenderBrain.MIN_DEFENDER_X;
					else
						x_toAssign = cbMess.ballX;

					player.getMem().currHome.x = x_toAssign;
				}


				player.messageClosest = "CBw" + player.getMem().uNum + "w" + (int)ballPos.x + "w" + (int)ballPos.y
						+ "w" + (int)player.getMem().getBall().getDistance();

			}
			else
			{
				//				agent.setWorldStateProperty(DefenderAgent.CLOSEST_TO_THE_BALL, false);

				player.messageClosest = "CBw" + cbMess.uNum + "w" + cbMess.ballX + "w" + cbMess.ballY
						+ "w" + cbMess.ballDistance;
			}
		}
		else
		{
			if(cbMess != null)
				player.messageClosest = "CBw" + cbMess.uNum + "w" + cbMess.ballX + "w" + cbMess.ballY
				+ "w" + cbMess.ballDistance;		
		}

		if(player.messageClosest.equals("void"))
		{
			player.ableSayingClosest = false;
		}
		else
			player.ableSayingClosest = true;
	}
}
