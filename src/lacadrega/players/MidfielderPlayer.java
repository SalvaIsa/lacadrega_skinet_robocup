package lacadrega.players;

import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;

import lacadrega.players.brains.DefenderBrain;
import lacadrega.players.brains.MidfielderBrain;
import skinet.framework.FullBackBrain;
import skinet.framework.Player;
import skinet.framework.Pos;
import skinet.framework.objInfos.TeamUnum;

public class MidfielderPlayer extends Player{

	public MidfielderBrain brain;
	
	public MidfielderPlayer() {
		super();
	}

	public MidfielderPlayer(String team)
	{
		super.getRoboClient().setTeam(team);
	}

	public void initMidfield(double x, double y) throws SocketException, UnknownHostException {

		this.initPlayer(x, y);

		brain = new MidfielderBrain(this);
	}

	@Override
	public void run() {

		while(true) {

			try {
//				getMem().cbMessageReceived = null;
//				messageClosest = "void";
				receiveInput();

			} catch (Throwable e) {
				System.out.println("Interrupt error in FullBack.run");
				e.printStackTrace();
			}

			if(getMem().current != null) {
				Pos pt;
				
				if(getMem().currHome == null)
					pt = mh.vSub(getMem().current, getMem().home);
				else
					pt = mh.vSub(getMem().current, getMem().currHome);
				
				try {
					
					if(ableSayingClosest)
						sayIfClosest_True(messageClosest);
					else
					{
						notSayingTurns = 0;
						saidThatClosest = 0;
					}
					
				} catch (UnknownHostException | InterruptedException e) {
					e.printStackTrace();
				}

				if(mh.mag(pt) > 0.5) {
					getMem().isHome = false;
				}
				else
					getMem().isHome = true;
			}
			else 
				System.out.println("Current is null");		
		}

	}
}
