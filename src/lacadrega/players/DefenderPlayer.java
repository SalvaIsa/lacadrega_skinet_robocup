package lacadrega.players;

import java.net.SocketException;
import java.net.UnknownHostException;

import lacadrega.players.brains.DefenderBrain;
import skinet.framework.Player;
import skinet.framework.Pos;
import skinet.framework.objInfos.TeamUnum;

public class DefenderPlayer extends Player {

	public double currentX = -1;
	public DefenderBrain brain;
	
	public DefenderPlayer() {
		super();
	}

	public DefenderPlayer(String team) {
		super.getRoboClient().setTeam(team);
	}

	public void initDefender(double x, double y) throws SocketException, UnknownHostException {
		this.initPlayer(x, y);

		brain = new DefenderBrain(this);
	}

	@Override
	public void run() {

		while (true) {

			try {
//				getMem().cbMessageReceived = null;
//				messageClosest = "void";
				receiveInput();

			} catch (Throwable e) {
				System.out.println("Interrupt error in Defender.run");
				e.printStackTrace();
			}

			if (getMem().current != null) {

				Pos pt;

				if(getMem().currHome == null)
					pt = mh.vSub(getMem().current, getMem().home);
				else
					pt = mh.vSub(getMem().current, getMem().currHome);

				try {
					
					if(ableSayingClosest)
						sayIfClosest_True(messageClosest);
					else
					{
						notSayingTurns = 0;
						saidThatClosest = 0;
					}
					
				} catch (UnknownHostException | InterruptedException e) {
					e.printStackTrace();
				}

				if (mh.mag(pt) > 0.5) {
					getMem().isHome = false;
				} else
					getMem().isHome = true;
			} else
				System.out.println("Current is null");
		}
	}
}
