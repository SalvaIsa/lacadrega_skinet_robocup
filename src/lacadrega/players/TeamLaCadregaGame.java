package lacadrega.players;

import java.net.SocketException;
import java.net.UnknownHostException;

import skinet.framework.Goalie;

/**
 * @file Game.java
 * @author Joel Tanzi*
 * @date 18 September 2011
 */

/**
 * @class Game This serves as a main class to assemble the RoboCup team and set
 *        them into action for the match.
 *
 */

public class TeamLaCadregaGame {

	static String team = "LaCad";
	static String team2 = "LaCad2";


	public static void main(String args[]) throws Exception {
		// Instantiate each player client

		//		int i = 0;
		//
		//		if(i == 0)
		startLeftTeam(team);
		//		else
		//			startRightTeam(team2);
	}

	public static void startLeftTeam(String team) throws SocketException, UnknownHostException {

		Goalie g1 = new Goalie(team);

		DefenderPlayer d2 = new DefenderPlayer(team);
		DefenderPlayer d3 = new DefenderPlayer(team);
		DefenderPlayer d4 = new DefenderPlayer(team);

		MidfielderPlayer m5 = new MidfielderPlayer(team);
		MidfielderPlayer m6 = new MidfielderPlayer(team);
		MidfielderPlayer m7 = new MidfielderPlayer(team);
		MidfielderPlayer m8 = new MidfielderPlayer(team);

		StrikerPlayer s9 = new StrikerPlayer(team);
		StrikerPlayer s10 = new StrikerPlayer(team);
		StrikerPlayer s11 = new StrikerPlayer(team);

		g1.initGoalie(-50, 0);
		//		// Thread.sleep(100);

		d2.initDefender(-40, 13);
		//		Thread.sleep(100);
		d3.initDefender(-40, 0);
		// Thread.sleep(100);
		d4.initDefender(-40, -13);
		// Thread.sleep(100);

		m5.initMidfield(-20, 24);
		// Thread.sleep(100);
		m6.initMidfield(-25, 8);
		// Thread.sleep(100);
		m7.initMidfield(-25, -8);
		// Thread.sleep(100);
		m8.initMidfield(-20, -24);
		// Thread.sleep(100);

		s9.initStriker(-10, 12);
		// Thread.sleep(100);
		s10.initStriker(-7, 0);
		// Thread.sleep(100);
		s11.initStriker(-10, -12);
		//	Thread.sleep(100);

		g1.start();
		d2.start();
		d3.start();
		d4.start();
		m5.start();
		m6.start();
		m7.start();
		m8.start();
		s9.start();
		s10.start();
		s11.start();
	}

	private static void startRightTeam(String team) throws SocketException, UnknownHostException {

		Goalie g1 = new Goalie(team);

		DefenderPlayer d2 = new DefenderPlayer(team);
		DefenderPlayer d3 = new DefenderPlayer(team);
		DefenderPlayer d4 = new DefenderPlayer(team);

		MidfielderPlayer m5 = new MidfielderPlayer(team);
		MidfielderPlayer m6 = new MidfielderPlayer(team);
		MidfielderPlayer m7 = new MidfielderPlayer(team);
		MidfielderPlayer m8 = new MidfielderPlayer(team);

		StrikerPlayer s9 = new StrikerPlayer(team);
		StrikerPlayer s10 = new StrikerPlayer(team);
		StrikerPlayer s11 = new StrikerPlayer(team);

		g1.initGoalie(-50, 0);
		// Thread.sleep(100);

		d2.initDefender(-40, 13);
		//		Thread.sleep(100);
		d3.initDefender(-40, 0);
		// Thread.sleep(100);
		d4.initDefender(-40, -13);
		// Thread.sleep(100);

		m5.initMidfield(-20, 24);
		// Thread.sleep(100);
		m6.initMidfield(-25, 8);
		// Thread.sleep(100);
		m7.initMidfield(-25, -8);
		// Thread.sleep(100);
		m8.initMidfield(-20, -24);
		// Thread.sleep(100);

		s9.initStriker(-10, 12);
		// Thread.sleep(100);
		s10.initStriker(-7, 0);
		// Thread.sleep(100);
		s11.initStriker(-10, -12);
		//	Thread.sleep(100);

		g1.start();
		d2.start();
		d3.start();
		d4.start();
		m5.start();
		m6.start();
		m7.start();
		m8.start();
		s9.start();
		s10.start();
		s11.start();
	}
}