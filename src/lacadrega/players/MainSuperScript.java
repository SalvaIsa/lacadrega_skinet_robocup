package lacadrega.players;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import skinet.framework.FullBackPlayer;
import skinet.framework.Goalie;
import skinet.framework.Player;

public class MainSuperScript {

	public static void main(String[] args) throws IOException {

		preOperations();

		startTeams();

	}

	public static void startTeams() {

		// Start first team
		new Thread(new Runnable() {

			@Override
			public void run() {

				try {
					startFirstTeam();
					// startSecondTeam();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();

	}

	public static void startFirstTeam() throws Exception {
		TeamLaCadregaGame.startLeftTeam("laCad");
	}

	public static void startSecondTeam() throws Exception {

		String teamName = "team2";

		// Instantiate each player client
		Player p1 = new Player(teamName);
		Player p2 = new Player(teamName);
		Player p3 = new Player(teamName);
		Player p4 = new Player(teamName);
		Player p5 = new Player(teamName);
		FullBackPlayer p6 = new FullBackPlayer(teamName);
		FullBackPlayer p7 = new FullBackPlayer(teamName);
		FullBackPlayer p8 = new FullBackPlayer(teamName);
		Goalie g9 = new Goalie(teamName);

		p1.initPlayer(-5, -25, "far_left_fwd");
		Thread.sleep(100);

		p2.initPlayer(-5, -10, "left_fwd");
		Thread.sleep(100);

		p3.initPlayer(-5, 10, "right_fwd");
		Thread.sleep(100);

		p4.initPlayer(-5, 25, "far_right_fwd");
		Thread.sleep(100);

		p5.initPlayer(-15, 0, "center_fwd");
		Thread.sleep(100);

		p6.initFullBack(-30, -25, "left_fb");
		Thread.sleep(100);

		p7.initFullBack(-30, 0, "center_fb");
		Thread.sleep(100);

		p8.initFullBack(-30, 25, "right_fb");
		Thread.sleep(100);

		g9.initGoalie(-40, 0);
		Thread.sleep(100);

		// Begin soccer match behaviors
		p1.start();
		p2.start();
		p3.start();
		p4.start();
		p5.start();
		p6.start();
		p7.start();
		p8.start();
		g9.start();
	}

	public static void preOperations() throws IOException {

		Runtime.getRuntime().exec(System.getProperty("user.dir") + "/robocup.sh");

		JFrame frame = new JFrame("robocup manager");
		frame.setSize(200, 100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel();

		JButton exit = new JButton("exit");
		exit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {

				try {
					Runtime.getRuntime().exec(System.getProperty("user.dir") + "/shutdownRobocup.sh");
				} catch (IOException e1) {
				}
				System.exit(0);
			}
		});
		panel.add(exit);

		frame.setContentPane(panel);
		frame.setVisible(true);
	}

}
