package lacadrega.players;

import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;

import skinet.framework.GoalieBrain;
import skinet.framework.Player;

public class GoaliePlayer extends Player{
	
	public GoaliePlayer() {
		super();
	}
	
	public GoaliePlayer(String team)
	{
		super.getRoboClient().setTeam(team);
	}
	
	public void initGoalie(double x, double y) throws SocketException, UnknownHostException {

		rc.dsock = new DatagramSocket();
		rc.initGoalie(getParser(), getMem());
		
		try {
			
			move(x,y);
			Thread.sleep(100);
			if(getMem().side.compareTo("r") == 0) {
				turn(180);
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
//		@SuppressWarnings("unused")
//		GoalieBrain b = new GoalieBrain(this);
	}

	public void run() {
		
		while(true) {
			try {
				receiveInput();
			} catch (InterruptedException e) {
				System.out.println("Interrupt error at Player.run");
				e.printStackTrace();
			}
		
		}
		
	}		
}
