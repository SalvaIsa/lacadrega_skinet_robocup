package lacadrega;

public class CBMessage {
	
	public int uNum;
	public int ballX;
	public int ballY;
	public int ballDistance;
	
	public CBMessage(int uNum, int ballX, int ballY, int ballD) {
		super();
	
		this.uNum = uNum;
		this.ballX = ballX;
		this.ballY = ballY;
		this.ballDistance = ballD;
	}

}
