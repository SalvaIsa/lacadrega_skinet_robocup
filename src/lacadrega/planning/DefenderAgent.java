package lacadrega.planning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;

import lacadrega.planning.actions.defender.Def_FreeArea;
import lacadrega.planning.actions.defender.Def_MarkOpponent;
import lacadrega.planning.actions.defender.Def_PassTheBall;
import lacadrega.planning.actions.defender.Def_RunTowardsTheBall;
import lacadrega.planning.goals.PlannerGoal;
import lg.planner.GoapAction;
import lg.planner.GoapAgent;
import lg.planner.IGoap;

public class DefenderAgent implements IGoap {

	public static final String FREE_FRIEND_FOR_PASSAGE = "FreeFriendForPassage";
	public static final String OPPONENT_CLOSEST_TO_BALL = "OpponentClosestToBall";
	public static final String FRIEND_CLOSEST_TO_BALL = "FriendClosestToBall";
	public static final String OPPONENT_IN_FRONT_FRIEND = "OpponentInFrontFriend";
	public static final String SEE_FAR_FRIEND = "SeeFarFriend";
	public static final String SEE_CLOSE_FRIEND = "SeeCloseFriend";
	
	public static final String SEE_FAR_OPPONENT = "SeeFarOpponent";
	public static final String SEE_CLOSE_OPPONENT = "SeeCloseOpponent";
	
	public static final String BALL_TO_OPPONENT = "BallToOpponent";
	public static final String BALL_TO_FRIENDS = "BallToFriends";
	public static final String CLOSEST_TO_THE_BALL = "ClosestToTheBall";
	public static final String HAVE_THE_BALL = "HaveTheBall";
	public static final String SEE_THE_BALL = "SeeTheBall";
	
	public static final String BALL_IN_DEFENSE = "BallInDefense";
	public static final String PLAYER_IN_DEFENSE = "PlayerInDefense";

	
	public static final String TO_MARK_OPP_IN_HOME = "ToMarkOppInHome";
	public static final String TO_MARK_OPP_IN_DEF_MAX_AREA = "ToMarkOppInDefMaxArea";
	
	public static final String DEFENDING_GOAL = "Defending";

	GoapAgent agent;

	HashMap<String, Boolean> worldState = new HashMap<>();
	PlannerGoal p;

	private Queue<GoapAction> goalActions;

	public DefenderAgent() {

		this.p = null;

		agent = new GoapAgent(this);
		initWorldState();
	}

	public PlannerGoal getGoalPlanner() {
		return p;
	}

	public void setGoalPlanner(PlannerGoal p) {
		this.p = p;
	}

	public void update() {

		goalActions = null;

		agent.update();
	}

	@Override
	public void planFailed(HashMap<String, Boolean> failedGoal) {
		// System.out.println("Plan failed");
	}

	@Override
	public void planFound(HashMap<String, Boolean> goal, Queue<GoapAction> actions) {
		System.out.println("Plan found " + GoapAgent.prettyPrint(actions));

		setGoalActions(actions);
	}

	@Override
	public void actionsFinished() {
		System.out.println("Actions finished");
	}

	@Override
	public void planAborted(GoapAction aborter) {
		System.out.println("Plan aborted");
	}

	@Override
	public List<GoapAction> getActions() {
		List<GoapAction> a = new ArrayList<>();

		a.add(new Def_RunTowardsTheBall());
		a.add(new Def_PassTheBall());
		a.add(new Def_FreeArea());
		a.add(new Def_MarkOpponent());

		return a;
	}

	@Override
	public HashMap<String, Boolean> getWorldState() {
		return worldState;
	}

	/*
	 * Le azioni di un agente sono sempre le stesse, cambiano le booleane in
	 * precondizione che ti fanno capire se l'Agente può fare determinate azioni
	 * oppure no;
	 */

	/*
	 * X = on going ; V = finita
	 * 
	 * 1- Passa la palla - X 2- Marca avversario senza palla 3- Vai verso la
	 * palla - X 4- Rimani in posizione 5- Torna in posizione 6- Vai
	 * sull'avversario con la palla 7- Avanza palla al piede 8- Spazza 9-
	 * Liberati per ricevere passaggio (Centro\Attaccante)
	 */

	public void initWorldState() {

		setWorldStateProperty(SEE_THE_BALL, false);
		setWorldStateProperty(HAVE_THE_BALL, false);
		setWorldStateProperty(CLOSEST_TO_THE_BALL, false);
		setWorldStateProperty(BALL_TO_FRIENDS, false);
		setWorldStateProperty(BALL_TO_OPPONENT, false);
		setWorldStateProperty(FRIEND_CLOSEST_TO_BALL, false);
		setWorldStateProperty(OPPONENT_CLOSEST_TO_BALL, false);
		
		setWorldStateProperty(SEE_CLOSE_OPPONENT, false);
		setWorldStateProperty(SEE_FAR_OPPONENT, false);
		
		setWorldStateProperty(SEE_CLOSE_FRIEND, false);
		setWorldStateProperty(SEE_FAR_FRIEND, false);
		setWorldStateProperty(OPPONENT_IN_FRONT_FRIEND, false);
		setWorldStateProperty(FREE_FRIEND_FOR_PASSAGE, false);
		setWorldStateProperty(BALL_IN_DEFENSE, false);
		setWorldStateProperty(PLAYER_IN_DEFENSE, false);

		setWorldStateProperty(TO_MARK_OPP_IN_DEF_MAX_AREA, false);
		setWorldStateProperty(TO_MARK_OPP_IN_HOME, false);
		
		setWorldStateProperty(DEFENDING_GOAL, false);
	}

	/*
	 * Qui di seguito ci sono i metodi Setters per le booleane del WorldState, a
	 * seconda delle condizioni del player nel campo; [ es: hoLaPalla,
	 * VedoAvversari, pallaAlCompagno, etc...
	 */

	public void setWorldStateProperty(String key, Boolean val) {
		this.worldState.put(key, val);
	}

	public boolean getWorldStateProperty(String key) {
		return this.worldState.get(key);
	}

	@Override
	public HashMap<String, Boolean> createGoalState() {
		return p.getGoalState();
	}

	public Queue<GoapAction> getGoalActions() {
		return goalActions;
	}

	public void setGoalActions(Queue<GoapAction> goalActions) {
		this.goalActions = goalActions;
	}

}
