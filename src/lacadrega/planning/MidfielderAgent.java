package lacadrega.planning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;

import lacadrega.planning.actions.midfielder.Mid_CrossInArea;
import lacadrega.planning.actions.midfielder.Mid_DribbleForward;
import lacadrega.planning.actions.midfielder.Mid_MarkOpponent;
import lacadrega.planning.actions.midfielder.Mid_PassTheBall;
import lacadrega.planning.actions.midfielder.Mid_PassToStriker;
import lacadrega.planning.actions.midfielder.Mid_RunTowardsTheBall;
import lacadrega.planning.actions.midfielder.Mid_ShootToGoal;
import lacadrega.planning.goals.PlannerGoal;
import lg.planner.GoapAction;
import lg.planner.GoapAgent;
import lg.planner.IGoap;

public class MidfielderAgent implements IGoap {

	public static final String SEE_FAR_OPPONENT = "SeeFarOpponent";
	public static final String SEE_CLOSE_OPPONENT = "SeeCloseOpponent";
	public static final String SEE_THE_BALL = "SeeTheBall";
	public static final String HAVE_THE_BALL = "HaveTheBall";
	public static final String CLOSEST_TO_THE_BALL = "ClosestToTheBall";
	public static final String BALL_TO_FRIENDS = "BallToFriends";
	public static final String BALL_IN_DEFENSE = "BallInDefense";
	public static final String FREE_FRIEND_FOR_PASSAGE = "FreeFriendForPassage";
	public static final String FREE_STRIKER_FOR_PASSAGE = "FreeStrikerForPassage";
	public static final String FRIEND_CLOSEST_TO_BALL = "FriendClosestToBall";
	public static final String OPPONENT_CLOSEST_TO_BALL = "OpponentClosestToBall";
	public static final String BALL_TO_OPPONENT = "BallToOpponent";
	public static final String OPPONENT_IN_FRONT_FRIEND = "OpponentInFrontFriend";
	
	public static final String BALL_IN_OPPONENT_CROSS_AREA = "BallInOpponentCrossArea";
	public static final String BALL_IN_OPPONENT_AREA = "BallInOpponentArea";

	public static final String FRIEND_GOING_TOWARDS_BALL = "FriendGoingTowardsBall";

	// PlannerGoal Condition
	public static final String ACTION_DONE = "ActionDone";

	GoapAgent agent;

	HashMap<String, Boolean> worldState = new HashMap<>();
	PlannerGoal p;

	private Queue<GoapAction> goalActions;

	public MidfielderAgent() {

		this.p = null;

		agent = new GoapAgent(this);
		initWorldState();
	}

	public PlannerGoal getGoalPlanner() {
		return p;
	}

	public void setGoalPlanner(PlannerGoal p) {
		this.p = p;
	}

	public void update() {
		goalActions = null;

		agent.update();
	}

	public void initWorldState() {

		setWorldStateProperty(MidfielderAgent.SEE_CLOSE_OPPONENT, false);
		setWorldStateProperty(MidfielderAgent.SEE_FAR_OPPONENT, false);
		setWorldStateProperty(MidfielderAgent.SEE_THE_BALL, false);
		setWorldStateProperty(MidfielderAgent.HAVE_THE_BALL, false);
		setWorldStateProperty(MidfielderAgent.CLOSEST_TO_THE_BALL, false);
		setWorldStateProperty(MidfielderAgent.BALL_TO_FRIENDS, false);
		setWorldStateProperty(MidfielderAgent.BALL_IN_DEFENSE, false);
		setWorldStateProperty(MidfielderAgent.FREE_FRIEND_FOR_PASSAGE, false);
		setWorldStateProperty(MidfielderAgent.FREE_STRIKER_FOR_PASSAGE, false);
		setWorldStateProperty(MidfielderAgent.FRIEND_CLOSEST_TO_BALL, false);
		setWorldStateProperty(MidfielderAgent.OPPONENT_CLOSEST_TO_BALL, false);
		setWorldStateProperty(MidfielderAgent.BALL_TO_OPPONENT, false);
		setWorldStateProperty(MidfielderAgent.OPPONENT_IN_FRONT_FRIEND, false);
		setWorldStateProperty(MidfielderAgent.BALL_IN_OPPONENT_CROSS_AREA, false);
		setWorldStateProperty(MidfielderAgent.BALL_IN_OPPONENT_AREA, false);
		setWorldStateProperty(MidfielderAgent.ACTION_DONE, false);
		setWorldStateProperty(MidfielderAgent.FRIEND_GOING_TOWARDS_BALL, false);
	}

	@Override
	public HashMap<String, Boolean> getWorldState() {
		return worldState;
	}

	@Override
	public HashMap<String, Boolean> createGoalState() {
		return p.getGoalState();
	}

	@Override
	public void planFailed(HashMap<String, Boolean> failedGoal) {
		// System.out.println("Plan failed");
	}

	@Override
	public void planFound(HashMap<String, Boolean> goal, Queue<GoapAction> actions) {
		System.out.println("Plan found " + GoapAgent.prettyPrint(actions));

		setGoalActions(actions);
	}

	@Override
	public void actionsFinished() {
		System.out.println("Actions finished");
	}

	@Override
	public void planAborted(GoapAction aborter) {
		System.out.println("Plan aborted");
	}

	@Override
	public List<GoapAction> getActions() {
		List<GoapAction> a = new ArrayList<>();

		a.add(new Mid_ShootToGoal());
		a.add(new Mid_CrossInArea());
		a.add(new Mid_PassTheBall());
		a.add(new Mid_PassToStriker());
		a.add(new Mid_RunTowardsTheBall());
		a.add(new Mid_DribbleForward());
//		a.add(new Mid_MarkOpponent());

		return a;
	}

	public void setWorldStateProperty(String key, Boolean val) {
		this.worldState.put(key, val);
	}

	public boolean getWorldStateProperty(String key) {
		return this.worldState.get(key);
	}

	public Queue<GoapAction> getGoalActions() {
		return goalActions;
	}

	public void setGoalActions(Queue<GoapAction> goalActions) {
		this.goalActions = goalActions;
	}
}
