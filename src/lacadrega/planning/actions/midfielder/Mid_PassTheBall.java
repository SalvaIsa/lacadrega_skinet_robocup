package lacadrega.planning.actions.midfielder;

import lacadrega.planning.MidfielderAgent;
import lacadrega.planning.actions.GeneralAction;
import lacadrega.players.brains.DefenderBrain;
import skinet.framework.Player;
import skinet.framework.objInfos.ObjBall;
import skinet.framework.objInfos.ObjPlayer;

public class Mid_PassTheBall extends GeneralAction {

	private String name = "Mid_PassTheBall";
	private Boolean passed = false;
	private Boolean shoot = false;

	public Mid_PassTheBall() {
		super(1);
		
		addPrecondition(MidfielderAgent.HAVE_THE_BALL, true);
		addPrecondition(MidfielderAgent.FREE_FRIEND_FOR_PASSAGE, true);

		addEffect(MidfielderAgent.HAVE_THE_BALL, false);
	}

	@Override
	public boolean perform(Object agent) {

		Player player = (Player) agent;

		ObjBall ball = player.getMem().getBall();
		if (ball == null) {
			if (shoot || passed)
				player.getAction().goCurrentHome();
		} else {
			double distToBall = ball.getDistance();
			ObjPlayer firstPlayerInSight = player.getMem().getFirstPlayerInSight();

			if (firstPlayerInSight != null && !passed) {

				if (distToBall <= DefenderBrain.BALL_POSSESSION_RANGE) {
					player.getAction().passBall(ball, firstPlayerInSight);
					passed = true;
					return true;
				} else {
					player.getAction().goCurrentHome();
				}

				// FIXME se ne deve andare
			} else if (firstPlayerInSight == null && !shoot) {
				player.getAction().kickToGoal(ball);
				if (distToBall <= DefenderBrain.BALL_POSSESSION_RANGE)
					shoot = true;
				return true;
			}
		}
		
		player.getAction().goCurrentHome();
		if (player.getMem().isHome)

		{
			done = true;
			return true;
		}

		return true;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mid_PassTheBall other = (Mid_PassTheBall) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
