package lacadrega.planning.actions.midfielder;

import lacadrega.planning.DefenderAgent;
import lacadrega.planning.MidfielderAgent;
import lacadrega.planning.actions.GeneralAction;
import lacadrega.players.MidfielderPlayer;
import lacadrega.players.brains.DefenderBrain;

public class Mid_MarkOpponent extends GeneralAction {

	public Mid_MarkOpponent() {
		super(1);

//		addPrecondition(DefenderAgent.CLOSEST_TO_THE_BALL, false);
		addPrecondition(MidfielderAgent.SEE_FAR_OPPONENT, true);
		addPrecondition(MidfielderAgent.HAVE_THE_BALL, false);
		addPrecondition(MidfielderAgent.BALL_TO_FRIENDS, false);
		addPrecondition(MidfielderAgent.BALL_TO_OPPONENT, true);
		
		// da vedere
		addEffect(MidfielderAgent.ACTION_DONE, true);
	}

	@Override
	public boolean perform(Object agent) {

		MidfielderPlayer player = (MidfielderPlayer) agent;

		if(player.brain.playerToMark == null|| 
				player.brain.agent.getWorldStateProperty(MidfielderAgent.BALL_TO_FRIENDS))
		{
			done = true;
			return true;

		}		
		else if(player.brain.playerToMark.getDistance() >= 1)
		{
			
			player.getAction().gotoPoint(player.brain.positionToMark);
		}

		return true;
	}

}
