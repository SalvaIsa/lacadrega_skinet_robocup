package lacadrega.planning.actions.midfielder;

import java.net.UnknownHostException;

import lacadrega.planning.MidfielderAgent;
import lacadrega.planning.actions.GeneralAction;
import skinet.framework.Player;

public class Mid_CrossInArea extends GeneralAction {

	private String name = "Mid_CrossInArea";

	public Mid_CrossInArea() {
		super(1);
		addPrecondition(MidfielderAgent.BALL_IN_OPPONENT_CROSS_AREA, true);
		addPrecondition(MidfielderAgent.BALL_IN_OPPONENT_AREA, false);
		addPrecondition(MidfielderAgent.HAVE_THE_BALL, true);

		addEffect(MidfielderAgent.ACTION_DONE, true);
		addEffect(MidfielderAgent.HAVE_THE_BALL, false);

	}

	@Override
	public boolean perform(Object agent) {

		Player player = (Player) agent;

		// kick(30, 45) sembra buono per driblare e entrare in area
		try {

			if(player.getMem().getPosition().y > 0)
				player.getRoboClient().kick(90, -90);
			else
				player.getRoboClient().kick(90, 90);

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			

		done = true;
		return true;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mid_CrossInArea other = (Mid_CrossInArea) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
