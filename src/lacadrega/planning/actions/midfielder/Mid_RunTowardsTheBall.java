package lacadrega.planning.actions.midfielder;

import java.net.UnknownHostException;

import lacadrega.planning.DefenderAgent;
import lacadrega.planning.MidfielderAgent;
import lacadrega.planning.actions.GeneralAction;
import lacadrega.players.brains.DefenderBrain;
import skinet.framework.Player;
import skinet.framework.Pos;
import skinet.framework.objInfos.ObjInfo;

public class Mid_RunTowardsTheBall extends GeneralAction {

	private String name = "Mid_RunTowardsTheBall";

	public Mid_RunTowardsTheBall() {
		super(1);
		addPrecondition(MidfielderAgent.HAVE_THE_BALL, false);
		addPrecondition(MidfielderAgent.CLOSEST_TO_THE_BALL, true);
		addPrecondition(MidfielderAgent.BALL_TO_FRIENDS, false);
		// addPrecondition(MidfielderAgent.BALL_IN_DEFENSE, false);

		addEffect(DefenderAgent.HAVE_THE_BALL, true);
	}

	@Override
	public boolean perform(Object agent) {

		Player player = (Player) agent;

		ObjInfo ball = player.getMem().getBall();
		if (ball == null) {
			try {
				player.getRoboClient().turn(Player.TURNING_DIRECTION);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
		double distToBall = ball.getDistance();

		if (distToBall <= DefenderBrain.BALL_POSSESSION_RANGE) {
			done = true;
			return true;
		}

		Pos ballPos = player.getMem().getBallPos(player.getMem().getBall());
		player.getAction().gotoPoint(ballPos);

		return true;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mid_RunTowardsTheBall other = (Mid_RunTowardsTheBall) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
