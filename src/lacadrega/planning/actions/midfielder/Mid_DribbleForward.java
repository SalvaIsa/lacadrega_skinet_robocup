package lacadrega.planning.actions.midfielder;

import java.net.UnknownHostException;

import lacadrega.planning.MidfielderAgent;
import lacadrega.planning.StrikerAgent;
import lacadrega.planning.actions.GeneralAction;
import lacadrega.players.MidfielderPlayer;
import lacadrega.players.brains.MidfielderBrain;
import skinet.framework.Player;

public class Mid_DribbleForward extends GeneralAction {

	private String name = "Mid_DribbleForward";

	public Mid_DribbleForward() {
		super(1);
		addPrecondition(MidfielderAgent.BALL_IN_OPPONENT_AREA, false);
		addPrecondition(MidfielderAgent.HAVE_THE_BALL, true);
		//		addPrecondition(MidfielderAgent.SEE_CLOSE_OPPONENT, false);

		addEffect(MidfielderAgent.BALL_IN_OPPONENT_AREA, true);
	}

	// power 13 dir 20 -> guud
	// power 15 dir 25 -> to check
	@Override
	public boolean perform(Object agent) {

		MidfielderPlayer player = (MidfielderPlayer) agent;

		if(player.brain.agent.getWorldStateProperty(StrikerAgent.SEE_CLOSE_OPPONENT))
		{
			double direction;

			if(player.getMem().getPosition().y > 0)
				direction = 25.0;
			else
				direction = -25.0;

			try {
				player.getRoboClient().kick(15.0, direction);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
			player.getAction().dribbleToGoal(player.getMem().getBall());

		// FIXME
		if (player.getMem().getPosition().x >= MidfielderBrain.OPPAREA_CLOSE_ENOUGH_X) {
			done = true;
			return true;
		}

		return true;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mid_DribbleForward other = (Mid_DribbleForward) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
