package lacadrega.planning.actions.midfielder;

import java.net.UnknownHostException;

import lacadrega.planning.MidfielderAgent;
import lacadrega.planning.actions.GeneralAction;
import skinet.framework.Player;
import skinet.framework.objInfos.ObjGoal;

public class Mid_ShootToGoal extends GeneralAction {

	private String name = "Mid_ShootToGoal";

	public Mid_ShootToGoal() {
		super(1);
		addPrecondition(MidfielderAgent.BALL_IN_OPPONENT_AREA, true);
		addPrecondition(MidfielderAgent.HAVE_THE_BALL, true);

		addEffect(MidfielderAgent.ACTION_DONE, true);
		addEffect(MidfielderAgent.HAVE_THE_BALL, false);

	}

	@Override
	public boolean perform(Object agent) {

		Player player = (Player) agent;

		ObjGoal oppG = player.getMem().getOppGoal();

		try {

			if(oppG == null)
			{
				if(player.getMem().getPosition().y > 0)
					player.getRoboClient().kick(15, -80);
				else
					player.getRoboClient().kick(15, 80);		
			}
			else
			{
				if(player.getMem().getPosition().y > 0)
					player.getRoboClient().kick(100, oppG.getDirection() + 10);
				else
					player.getRoboClient().kick(100, oppG.getDirection() - 10);
			}
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		done = true;
		return true;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mid_ShootToGoal other = (Mid_ShootToGoal) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
