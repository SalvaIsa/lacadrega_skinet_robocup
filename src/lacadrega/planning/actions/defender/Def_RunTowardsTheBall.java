package lacadrega.planning.actions.defender;

import java.net.UnknownHostException;

import lacadrega.planning.DefenderAgent;
import lacadrega.planning.actions.GeneralAction;
import lacadrega.players.brains.DefenderBrain;
import skinet.framework.Player;
import skinet.framework.Pos;
import skinet.framework.objInfos.ObjInfo;

public class Def_RunTowardsTheBall extends GeneralAction {

	private String name = "Def_RunTowardsTheBall";

	public Def_RunTowardsTheBall() {
		super(1);
		addPrecondition(DefenderAgent.HAVE_THE_BALL, false);
		addPrecondition(DefenderAgent.CLOSEST_TO_THE_BALL, true);
		addPrecondition(DefenderAgent.BALL_TO_FRIENDS, false);
		addPrecondition(DefenderAgent.BALL_IN_DEFENSE, true);
		//		addPrecondition(DefenderAgent.PLAYER_IN_DEFENSE, true);

		addEffect(DefenderAgent.HAVE_THE_BALL, true);
	}

	@Override
	public boolean perform(Object agent) {

		Player player = (Player) agent;

		ObjInfo ball = player.getMem().getBall();
		if (ball == null) {
			try {
				player.getRoboClient().turn(Player.TURNING_DIRECTION);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
		double distToBall = ball.getDistance();

		if (distToBall <= DefenderBrain.BALL_POSSESSION_RANGE) {
			done = true;
			return true;
		}

		Pos ballPos = player.getMem().getBallPos(player.getMem().getBall());
		player.getAction().gotoPoint(ballPos);

		return true;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Def_RunTowardsTheBall other = (Def_RunTowardsTheBall) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
