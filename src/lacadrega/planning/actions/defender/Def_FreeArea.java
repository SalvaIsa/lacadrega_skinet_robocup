package lacadrega.planning.actions.defender;

import lacadrega.planning.DefenderAgent;
import lacadrega.planning.actions.GeneralAction;
import lacadrega.players.brains.DefenderBrain;
import skinet.framework.Player;
import skinet.framework.objInfos.ObjBall;

public class Def_FreeArea extends GeneralAction {

	private String name = "Def_FreeArea";
	private boolean shoot = false;

	public Def_FreeArea() {
		super(1);
		addPrecondition(DefenderAgent.HAVE_THE_BALL, true);
		addPrecondition(DefenderAgent.FREE_FRIEND_FOR_PASSAGE, false);
		addPrecondition(DefenderAgent.BALL_IN_DEFENSE, true);
		//		addPrecondition(DefenderAgent.PLAYER_IN_DEFENSE, true);

		addEffect(DefenderAgent.HAVE_THE_BALL, false);
		addEffect(DefenderAgent.DEFENDING_GOAL, true);
	}

	@Override
	public boolean perform(Object agent) {
		Player player = (Player) agent;

		ObjBall ball = player.getMem().getBall();

		if (ball == null) {

			player.getAction().goCurrentHome();

		} else if (!shoot) {

			double distToBall = ball.getDistance();

			if (distToBall <= DefenderBrain.BALL_POSSESSION_RANGE) {

				player.getAction().kickToGoal(player.getMem().getBall());
			}
			shoot = true;
		} else {
			player.getAction().goCurrentHome();		
		}

		if (player.getMem().isHome)

		{
			done = true;
			return true;
		}

		return true;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Def_FreeArea other = (Def_FreeArea) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
