package lacadrega.planning.actions.defender;

import java.net.UnknownHostException;

import lacadrega.planning.DefenderAgent;
import lacadrega.planning.actions.GeneralAction;
import lacadrega.players.DefenderPlayer;
import lacadrega.players.brains.DefenderBrain;
import skinet.framework.Player;
import skinet.framework.Pos;
import skinet.framework.objInfos.ObjBall;

public class Def_MarkOpponent extends GeneralAction {

	public Def_MarkOpponent() {
		super(1);

		addPrecondition(DefenderAgent.CLOSEST_TO_THE_BALL, false);
		addPrecondition(DefenderAgent.TO_MARK_OPP_IN_DEF_MAX_AREA, true);
		addPrecondition(DefenderAgent.TO_MARK_OPP_IN_HOME, true);
		addPrecondition(DefenderAgent.SEE_FAR_OPPONENT, true);
		addPrecondition(DefenderAgent.HAVE_THE_BALL, false);
		addPrecondition(DefenderAgent.BALL_TO_FRIENDS, false);

		// da vedere
		addEffect(DefenderAgent.DEFENDING_GOAL, true);
	}

	@Override
	public boolean perform(Object agent) {

		DefenderPlayer player = (DefenderPlayer) agent;

		ObjBall b = player.getMem().getBall();

		if(b == null)
			try {
				player.getRoboClient().turn(Player.TURNING_DIRECTION);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else
		{
			if(b.getDistance() <= DefenderBrain.BALL_POSSESSION_RANGE)
				player.getAction().kickToGoal(b);

			if(b!=null && b.getDistance() <= player.brain.playerToMark.getDistance() + 5 ){
				player.getAction().marking_gotoPoint(player.getMem().getBallPos(b));
			}
			else
				//			if(player.brain.playerToMark.getDistance() >= 1)
				//			{
				player.getAction().marking_gotoPoint(player.brain.positionToMark);
			//			}

			if(player.brain.playerToMark == null || 
					player.brain.positionToMark.x >= DefenderBrain.MAX_DEFENDER_X 
					|| !(player.brain.positionToMark.x <= player.getMem().currHome.x + DefenderBrain.TO_MARK_RANGE)
					|| !(player.brain.positionToMark.x >= player.getMem().currHome.x - DefenderBrain.TO_MARK_RANGE) )
			{
				done = true;
				return true;
			}		

		}

		return true;
	}

	//	double distOppBall = 200;
	//
	//		if(b!=null)
	//		{
	//			double bSquare = player.brain.playerToMark.getDistance() * player.brain.playerToMark.getDistance();
	//			double cSquare = b.getDistance() * b.getDistance();
	//			double alfa = b.getDirection() + Math.abs(player.brain.playerToMark.getDirection());
	//			double aSquare = bSquare + cSquare - 2 * player.brain.playerToMark.getDistance() * b.getDistance() * Math.cos(Math.toRadians(alfa));
	//			distOppBall = Math.sqrt(aSquare);
	//		}

	//		if(distOppBall <= 1.1 || b.getDistance() <= 2)
	//	if(!player.brain.agent.getWorldStateProperty(DefenderAgent.OPPONENT_CLOSEST_TO_BALL))
	//		{
	//			done = true;
	//			return true;
	//		}
}
