package lacadrega.planning.actions.striker;

import java.net.UnknownHostException;

import lacadrega.planning.StrikerAgent;
import lacadrega.planning.actions.GeneralAction;
import skinet.framework.Player;
import skinet.framework.objInfos.ObjGoal;

public class Strk_ShootToGoal extends GeneralAction {

	private String name = "Strk_ShootToGoal";

	public Strk_ShootToGoal() {
		super(1);
		//TODO shoot safe and not safe 
		//		addPrecondition(StrikerAgent.SEE_OPPONENT_GOAL,true);
		addPrecondition(StrikerAgent.BALL_IN_OPPONENT_AREA, true);
		addPrecondition(StrikerAgent.HAVE_THE_BALL, true);

		addEffect(StrikerAgent.SHOOT, true);
		addEffect(StrikerAgent.HAVE_THE_BALL, false);

	}

	@Override
	public boolean perform(Object agent) {

		Player player = (Player) agent;

		ObjGoal oppG = player.getMem().getOppGoal();

		try {

			if(oppG == null)
			{
				if(player.getMem().getPosition().y > 0)
					player.getRoboClient().kick(15, -80);
				else
					player.getRoboClient().kick(15, 80);		
			}
			else
			{
				if(player.getMem().getPosition().y > 0)
					player.getRoboClient().kick(100, oppG.getDirection() + 10);
				else
					player.getRoboClient().kick(100, oppG.getDirection() - 10);
			}

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		done = true;
		return true;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Strk_ShootToGoal other = (Strk_ShootToGoal) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
