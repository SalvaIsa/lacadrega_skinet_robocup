package lacadrega.planning.actions.striker;

import java.net.UnknownHostException;

import lacadrega.planning.StrikerAgent;
import lacadrega.planning.actions.GeneralAction;
import skinet.framework.Player;

public class Strk_DribbleToArea extends GeneralAction {

	private String name = "Strk_DribbleToArea";

	public Strk_DribbleToArea() {
		super(1);
		
		// TODO shoot safe and not safe 
		// addPrecondition(StrikerAgent.SEE_OPPONENT_GOAL,true);
		addPrecondition(StrikerAgent.BALL_IN_OPPONENT_AREA, false);
		addPrecondition(StrikerAgent.BALL_IN_OPPONENT_DRIBBLE_AREA, true);		
		addPrecondition(StrikerAgent.HAVE_THE_BALL, true);

		addEffect(StrikerAgent.SHOOT, true);
		addEffect(StrikerAgent.HAVE_THE_BALL, false);

	}

	@Override
	public boolean perform(Object agent) {

		Player player = (Player) agent;

		try {

			if(player.getMem().getPosition().y > 0)
				player.getRoboClient().kick(20, -90);
			else
				player.getRoboClient().kick(20, 90);

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		done = true;
		return true;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Strk_DribbleToArea other = (Strk_DribbleToArea) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
