package lacadrega.planning.actions.striker;

import lacadrega.planning.StrikerAgent;
import lacadrega.planning.actions.GeneralAction;
import lacadrega.players.brains.DefenderBrain;
import skinet.framework.Player;
import skinet.framework.objInfos.ObjBall;
import skinet.framework.objInfos.ObjPlayer;

public class Strk_PassTheBall extends GeneralAction {

	private String name = "Strk_PassTheBall";
	private Boolean passed = false;
	private Boolean shoot = false;

	public Strk_PassTheBall() {
		super(1);
		addPrecondition(StrikerAgent.HAVE_THE_BALL, true);
		addPrecondition(StrikerAgent.FREE_FRIEND_FOR_PASSAGE, true);

		addEffect(StrikerAgent.HAVE_THE_BALL, false);
	}

	@Override
	public boolean perform(Object agent) {

		Player player = (Player) agent;

		ObjBall ball = player.getMem().getBall();
		if (ball == null) {
			if (shoot || passed)
				player.getAction().goCurrentHome();
		} else {
			double distToBall = ball.getDistance();
			ObjPlayer firstPlayerInSight = player.getMem().getFirstPlayerInSight();

			if (firstPlayerInSight != null && !passed) {

				if (distToBall <= DefenderBrain.BALL_POSSESSION_RANGE) {
					player.getAction().passBall(ball, firstPlayerInSight);
					passed = true;
					return true;
				} else {
					player.getAction().goCurrentHome();
				}

				// FIXME se ne deve andare
			} else if (firstPlayerInSight == null && !shoot) {
				player.getAction().kickToGoal(ball);
				if (distToBall <= DefenderBrain.BALL_POSSESSION_RANGE)
					shoot = true;
				return true;
			}
		}
		player.getAction().goCurrentHome();
		
		if (player.getMem().isHome)
		{
			done = true;
			return true;
		}

		return true;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Strk_PassTheBall other = (Strk_PassTheBall) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
