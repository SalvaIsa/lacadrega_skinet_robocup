package lacadrega.planning.actions.striker;

import java.util.ArrayList;

import javax.jws.soap.InitParam;

import skinet.framework.Player;
import skinet.framework.Pos;
import skinet.framework.objInfos.ObjBall;
import skinet.framework.objInfos.ObjPlayer;
import lacadrega.planning.DefenderAgent;
import lacadrega.planning.StrikerAgent;
import lacadrega.planning.actions.GeneralAction;
import lacadrega.players.StrikerPlayer;
import lacadrega.players.brains.DefenderBrain;
import lacadrega.players.brains.StrikerBrain;

public class Strk_PassOneTwo extends GeneralAction {
	
	private String name = "Strk_PassOneTwo";
	private Boolean passed = false;
	
	public Strk_PassOneTwo() {
		super(1);
		addPrecondition(StrikerAgent.DIR_TEAMMATE_GOOD, true);
		addPrecondition(DefenderAgent.SEE_CLOSE_OPPONENT, true);
		addPrecondition(DefenderAgent.SEE_CLOSE_FRIEND, true);
		addPrecondition(StrikerAgent.HAVE_THE_BALL, true);

		addEffect(StrikerAgent.HAVE_THE_BALL, false);
		addEffect(StrikerAgent.BALL_IN_OPPONENT_AREA, true);

	}

	@Override
	public boolean perform(Object agent) {
		Player player = (Player) agent;

		ObjBall ball = player.getMem().getBall();
		Pos initPos = player.getPosition();
		if (ball == null) {
			if (passed)
				player.getAction().goCurrentHome();
		} else {
			double distToBall = ball.getDistance();
			/*
			 * passa al gioc2
			 */
			ObjPlayer friend = ((StrikerPlayer) player).brain.friendOneTwo;
			ObjPlayer opponent = ((StrikerPlayer) player).brain.opponentOneTwo; 
			
			if (friend != null && opponent != null && !passed) {

				if (distToBall <= DefenderBrain.BALL_POSSESSION_RANGE) {
					player.getAction().passBall(ball, friend);
					passed = true;
					/*
					 * avanza verso la porta
					 */
					
					Pos forwardPos = new Pos(initPos.x+StrikerBrain.FORWARD_DISTANCE, initPos.y);
					player.getAction().gotoPoint(forwardPos);
					return true;
				} else {
					player.getAction().goCurrentHome();
				}

			} 
		}
		if (player.getPosition().x >= initPos.x + StrikerBrain.FORWARD_DISTANCE - 2 && 
				player.getPosition().x <= initPos.x+StrikerBrain.FORWARD_DISTANCE + 2)
		{
			done = true;
			return true;
		}

		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Strk_PassOneTwo other = (Strk_PassOneTwo) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
