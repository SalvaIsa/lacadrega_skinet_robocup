package lacadrega.planning.actions.striker;


import java.net.UnknownHostException;

import lacadrega.planning.StrikerAgent;
import lacadrega.planning.actions.GeneralAction;
import lacadrega.players.StrikerPlayer;
import lacadrega.players.brains.StrikerBrain;
import skinet.framework.Player;

public class Strk_DribbleForward extends GeneralAction {

	private String name = "Strk_DribbleForward";

	public Strk_DribbleForward() {
		super(1);
		addPrecondition(StrikerAgent.BALL_IN_OPPONENT_AREA, false);
		addPrecondition(StrikerAgent.HAVE_THE_BALL, true);
		//		addPrecondition(StrikerAgent.SEE_CLOSE_OPPONENT, false);

		addEffect(StrikerAgent.BALL_IN_OPPONENT_AREA, true);
	}

	@Override
	public boolean perform(Object agent) {

		StrikerPlayer player = (StrikerPlayer) agent;

		if(player.brain.agent.getWorldStateProperty(StrikerAgent.SEE_CLOSE_OPPONENT))
		{
			double direction;
			
			if(player.getMem().getPosition().y > 0)
				direction = 20.0;
			else
				direction = -20.0;
			
			try {
				player.getRoboClient().kick(13.0, direction);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
			player.getAction().dribbleToGoal(player.getMem().getBall());

		// FIXME
		if (player.getMem().getPosition().x >= StrikerBrain.OPPAREA_STRIKER_CLOSE_ENOUGH_X) {
			done = true;
			return true;
		}

		return true;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Strk_DribbleForward other = (Strk_DribbleForward) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
