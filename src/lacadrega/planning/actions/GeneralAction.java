package lacadrega.planning.actions;

import lg.planner.GoapAction;

public abstract class GeneralAction extends GoapAction {

	protected boolean done = false;

	public GeneralAction(float cost) {
		super(cost);
	}

	@Override
	public void reset() {
		done = false;
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public boolean checkProceduralPrecondition(Object agent) {
		return true;
	}

}
