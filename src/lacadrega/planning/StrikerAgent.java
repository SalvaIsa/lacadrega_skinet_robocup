package lacadrega.planning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;

import lacadrega.planning.actions.striker.Strk_DribbleForward;
import lacadrega.planning.actions.striker.Strk_DribbleToArea;
import lacadrega.planning.actions.striker.Strk_PassOneTwo;
import lacadrega.planning.actions.striker.Strk_PassTheBall;
import lacadrega.planning.actions.striker.Strk_PassToStriker;
import lacadrega.planning.actions.striker.Strk_RunTowardsTheBall;
import lacadrega.planning.actions.striker.Strk_ShootToGoal;
import lacadrega.planning.goals.PlannerGoal;
import lg.planner.GoapAction;
import lg.planner.GoapAgent;
import lg.planner.IGoap;

public class StrikerAgent implements IGoap {

	public static final String SEE_FAR_OPPONENT = "SeeFarOpponent";
	public static final String SEE_CLOSE_OPPONENT = "SeeCloseOpponent";
	
	public static final String SEE_THE_BALL = "SeeTheBall";
	public static final String SEE_OPPONENT_GOAL = "SeeOpponentGoal";
	public static final String HAVE_THE_BALL = "HaveTheBall";
	
	public static final String CLOSEST_TO_THE_BALL = "ClosestToTheBall";
	public static final String BALL_TO_FRIENDS = "BallToFriends";
	public static final String FREE_FRIEND_FOR_PASSAGE = "FreeFriendForPassage";
	public static final String FREE_STRIKER_FOR_PASSAGE = "FreeStrikerForPassage";
	
	public static final String FRIEND_CLOSEST_TO_BALL = "FriendClosestToBall";
	public static final String OPPONENT_CLOSEST_TO_BALL = "OpponentClosestToBall";
	public static final String BALL_TO_OPPONENT = "BallToOpponent";
	
	public static final String OPPONENT_IN_FRONT_FRIEND = "OpponentInFrontFriend";
	
	public static final String BALL_IN_OPPONENT_DRIBBLE_AREA = "BallInOpponentDribbleArea";
	public static final String BALL_IN_OPPONENT_AREA = "BallInOpponentArea";
	
	public static final String DIR_TEAMMATE_GOOD = "DirTeammateGood";

	// PlannerGoal Condition
	public static final String SHOOT = "SHOOT";

	GoapAgent agent;

	HashMap<String, Boolean> worldState = new HashMap<>();
	PlannerGoal p;

	private Queue<GoapAction> goalActions;

	public StrikerAgent() {

		this.p = null;

		agent = new GoapAgent(this);
		initWorldState();
	}

	public PlannerGoal getGoalPlanner() {
		return p;
	}

	public void setGoalPlanner(PlannerGoal p) {
		this.p = p;
	}

	public void update() {
		goalActions = null;

		agent.update();
	}

	public void initWorldState() {

		setWorldStateProperty(StrikerAgent.SEE_CLOSE_OPPONENT, false);
		setWorldStateProperty(StrikerAgent.SEE_FAR_OPPONENT, false);
		setWorldStateProperty(StrikerAgent.SEE_THE_BALL, false);
		setWorldStateProperty(StrikerAgent.SEE_OPPONENT_GOAL, false);
		setWorldStateProperty(StrikerAgent.HAVE_THE_BALL, false);
		setWorldStateProperty(StrikerAgent.CLOSEST_TO_THE_BALL, false);
		setWorldStateProperty(StrikerAgent.BALL_TO_FRIENDS, false);
		setWorldStateProperty(StrikerAgent.FREE_FRIEND_FOR_PASSAGE, false);
		setWorldStateProperty(StrikerAgent.FREE_STRIKER_FOR_PASSAGE, false);
		setWorldStateProperty(StrikerAgent.FRIEND_CLOSEST_TO_BALL, false);
		setWorldStateProperty(StrikerAgent.OPPONENT_CLOSEST_TO_BALL, false);
		setWorldStateProperty(StrikerAgent.BALL_TO_OPPONENT, false);
		setWorldStateProperty(StrikerAgent.OPPONENT_IN_FRONT_FRIEND, false);
		setWorldStateProperty(StrikerAgent.BALL_IN_OPPONENT_DRIBBLE_AREA, false);
		setWorldStateProperty(StrikerAgent.BALL_IN_OPPONENT_AREA, false);
		//FIXME perchè diamine era true?
		setWorldStateProperty(StrikerAgent.SHOOT, false);
		setWorldStateProperty(StrikerAgent.DIR_TEAMMATE_GOOD, false);
	}

	@Override
	public HashMap<String, Boolean> getWorldState() {
		return worldState;
	}

	@Override
	public HashMap<String, Boolean> createGoalState() {
		return p.getGoalState();
	}

	@Override
	public void planFailed(HashMap<String, Boolean> failedGoal) {
		// System.out.println("Plan failed");
	}

	@Override
	public void planFound(HashMap<String, Boolean> goal, Queue<GoapAction> actions) {
		System.out.println("Plan found " + GoapAgent.prettyPrint(actions));

		setGoalActions(actions);
	}

	@Override
	public void actionsFinished() {
		System.out.println("Actions finished");
	}

	@Override
	public void planAborted(GoapAction aborter) {
		System.out.println("Plan aborted");
	}

	@Override
	public List<GoapAction> getActions() {
		List<GoapAction> a = new ArrayList<>();

		a.add(new Strk_DribbleToArea());
		a.add(new Strk_ShootToGoal());
		a.add(new Strk_PassTheBall());
		a.add(new Strk_PassToStriker());
		a.add(new Strk_RunTowardsTheBall());
		a.add(new Strk_DribbleForward());
		//		a.add(new Strk_PassOneTwo());
		
		return a;
	}

	public void setWorldStateProperty(String key, Boolean val) {
		this.worldState.put(key, val);
	}

	public boolean getWorldStateProperty(String key) {
		return this.worldState.get(key);
	}

	public Queue<GoapAction> getGoalActions() {
		return goalActions;
	}

	public void setGoalActions(Queue<GoapAction> goalActions) {
		this.goalActions = goalActions;
	}
}
