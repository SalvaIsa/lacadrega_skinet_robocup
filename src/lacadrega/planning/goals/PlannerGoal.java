package lacadrega.planning.goals;

import java.util.HashMap;

public interface PlannerGoal {
	public HashMap<String, Boolean> getGoalState();
}
