package lacadrega.planning.goals;

import java.util.HashMap;

import lacadrega.planning.DefenderAgent;

public class DefenderGoal implements PlannerGoal {

	HashMap<String, Boolean> goalState = new HashMap<>();

	public DefenderGoal() {

		this.goalState.put(DefenderAgent.DEFENDING_GOAL, true);

	}

	@Override
	public HashMap<String, Boolean> getGoalState() {
		return goalState;
	}

}