package lacadrega.planning.goals;

import java.util.HashMap;

import lacadrega.planning.MidfielderAgent;

public class MidfielderGoal implements PlannerGoal {

	HashMap<String, Boolean> goalState = new HashMap<>();

	public MidfielderGoal() {

		this.goalState.put(MidfielderAgent.ACTION_DONE, true);

	}

	@Override
	public HashMap<String, Boolean> getGoalState() {
		return goalState;
	}

}
