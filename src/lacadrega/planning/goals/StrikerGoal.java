package lacadrega.planning.goals;

import java.util.HashMap;

import lacadrega.planning.StrikerAgent;

public class StrikerGoal implements PlannerGoal {

	HashMap<String, Boolean> goalState = new HashMap<>();

	public StrikerGoal() {

		this.goalState.put(StrikerAgent.SHOOT, true);

	}

	@Override
	public HashMap<String, Boolean> getGoalState() {
		return goalState;
	}
}
