package skinet.framework.objInfos;

/**
 * @class ObjGoal
 * 
 * container for the goal ObjInfo,
 * 
 */
public class ObjGoal extends ObjInfo {

	public ObjGoal() {
		super("goal");
	}
}