package skinet.framework.objInfos;

/**
 * @class ObjLine
 * 
 * container for line ObjInfo
 *
 */
public class ObjLine extends ObjInfo {
	
	public ObjLine() {
		super("line");
	}
}
