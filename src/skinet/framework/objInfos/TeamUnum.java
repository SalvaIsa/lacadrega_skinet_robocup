package skinet.framework.objInfos;

public class TeamUnum {

	public int uNum;
	public String team;
	public double distance;
	
	public TeamUnum(int uNum, String team, double dist) {
		this.uNum = uNum;
		this.team = team;
		this.distance = dist;
	}
}
