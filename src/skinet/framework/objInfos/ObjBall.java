package skinet.framework.objInfos;

 /**
 * @class ObjBall
 * 
 * container for the ball ObjInfo,
 * 
 */
public class ObjBall extends ObjInfo {

	public ObjBall() {
		super("ball");
	}
}
