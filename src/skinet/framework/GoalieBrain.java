package skinet.framework;
import java.net.UnknownHostException;
import java.util.ArrayList;

import lacadrega.planning.DefenderAgent;
import lacadrega.players.brains.DefenderBrain;
import skinet.framework.objInfos.ObjBall;
import skinet.framework.objInfos.ObjPlayer;
import skinet.framework.objInfos.TeamUnum;



/**
 * @file Brain.java
 * @author Joel *
 */

/**@class Brain
 * The brain serves as a place to store the Player modes, marked players for
 * various functions, and a set of strategies for player actions.
 */
public class GoalieBrain extends Thread {

	private Mode currentMode = new Mode();	
	private Action actions = new Action();
	private String marked_team;
	private String marked_unum;
	public Goalie g;
	public Memory m;
	private boolean arrived = false;
	private boolean isGoing = false;
	private ArrayList<Pos> positions = new ArrayList<Pos>();
	private double endX;
	private double endY;
	private double m2;
	private final double ZONE_RANGE = 1.5f;
	private final double CAN_SHOOT_POINT = -25.0;
	private final double UP_AREA_MARGIN = -9.0;
	private final double DOWN_AREA_MARGIN = 9.0;
	private final double IS_SHOOTING_DIFFERENCE = 2.0;
	private final double EXIT_RANGE = 12.0;
	private final double AREA_RANGE = -36.0;
	public MathHelp mathHelp = new MathHelp();
	private boolean isOurHome = false;
	private int turning_steps = 1;
	private boolean turning_checking = false;

	/**
	 * Default constructor
	 */
	public GoalieBrain() {
		super();
	}

	public GoalieBrain(Goalie g) {
		this.g = g;
		//		try {
		//			g.turn(25.0);
		//		} catch (UnknownHostException | InterruptedException e) {
		//			e.printStackTrace();
		//		}
		start();
	}

	/**
	 * @return the actions
	 */
	public Action getActions() {
		return actions;
	}

	/**
	 * @param actions the actions to set
	 */
	public void setActions(Action actions) {
		this.actions = actions;
	}

	/**
	 * Constructor
	 * @param currentMode
	 */
	public GoalieBrain(Mode currentMode) {
		super();
		this.currentMode = currentMode;
	}

	/**
	 * @return the currentMode
	 */
	public Mode getCurrentMode() {
		return currentMode;
	}

	/**
	 * Sets the player mode to defensive
	 */
	public void setDefensive() {
		this.currentMode.setModename("D");
	}

	/**
	 * Sets the player mode to be offensive
	 */
	public void setOffensive() {
		this.currentMode.setModename("O");
	}

	/**
	 * @return the marked_team
	 */
	public String getMarked_team() {
		return marked_team;
	}

	/**
	 * @param marked_team the marked_team to set
	 */
	public void setMarked_team(String marked_team) {
		this.marked_team = marked_team;
	}

	/**
	 * @return the marked_unum
	 */
	public String getMarked_unum() {
		return marked_unum;
	}

	/**
	 * @param marked_unum the marked_unum to set
	 */
	public void setMarked_unum(String marked_unum) {
		this.marked_unum = marked_unum;
	}

	/**
	 * The Brain thread run method. It causes the Goalie to exhibit soccer behaviors.
	 * 
	 * @post Goalie will perform Goalie functions during match.
	 */
	
	private void turnAndGoHome() {
			if (g.getMem().isHome)
				try {
					if (!g.getMem().isObjVisible("ball"))
					{
						g.getRoboClient().turn(Player.TURNING_DIRECTION);
					}
					isOurHome = true;
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
			else {

				if(!turning_checking)
				{
					g.getAction().goCurrentHome();

					turning_steps++;
					if(turning_steps >= 2)
						turning_checking = true;
				}

				if ( turning_checking )
				{
					try {

						if (!g.getMem().isObjVisible("ball"))
							g.getRoboClient().turn(Player.TURNING_DIRECTION);
						else
							turning_steps = 0;

						turning_steps--;

						if(turning_steps <= 0)
							turning_checking = false;

					} catch (UnknownHostException e) {
						e.printStackTrace();
					}
				}


			}
	}
	
	public void run() {

		Pos uppergoalkick = new Pos(-47.36, -8.74);
		Pos lowergoalkick = new Pos(-47.36, 8.74);

		while (true) {

			try {
				Thread.sleep(100);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			//TODO torna a casa se non vedo la palla o se è fuori area
			
			/*if (!g.getMem().isObjVisible("ball")){
				try {
					g.turn(90.0);
					g.getAction().goInitHome();
				} catch (UnknownHostException | InterruptedException e) {
					e.printStackTrace();
				}
			}*/
			
			
			if (isGoing) {
				/*
				 * sono arrivato al punto target
				 */
				if (g.getPosition().x <= endX+ZONE_RANGE && g.getPosition().x >= endX-ZONE_RANGE &&
						g.getPosition().y <= endY+ZONE_RANGE && g.getPosition().y >= endY-ZONE_RANGE) {
					System.out.println("sono arrivato");
					try {
						ObjBall ball = g.getMem().getBall();
						if (ball != null){
						g.catchball(ball.getDirection());
						g.getAction().kickToGoal(ball);
						}
						isGoing = false;
					} catch (UnknownHostException e1) {
						e1.printStackTrace();
					}
				}
				/*
				 * continuo ad andare al punto target
				 */
				else {
					if (g.getPosition().y >= DOWN_AREA_MARGIN || g.getPosition().y <= UP_AREA_MARGIN){
						isGoing = false;
					}
					else{
						g.getAction().gotoPoint(new Pos(endX,endY));
						System.out.println("continuo ad andare al punto target");
					}
				}
			}
			
			else if (g.getMem().isObjVisible("ball") && !isGoing) {
				Pos ballPos = g.getMem().getBallPos(g.getMem().getBall());
				ObjBall ball = g.getMem().getBall();
				double ballDistance = ball.getDistance();
				if (ballPos.x > AREA_RANGE)
					g.getAction().goInitHome();
				else if (ballDistance <= EXIT_RANGE && ballPos.x <= AREA_RANGE){
					System.out.println("palla vicina, vado");
					g.getAction().gotoPoint(ballPos);
					if (ballDistance <= DefenderBrain.BALL_POSSESSION_RANGE)
						g.getAction().kickToGoal(ball);
				}
				else if (ballPos.x < CAN_SHOOT_POINT) {
					positions.add(ballPos);
					int size = positions.size();

					if (size > 100) {
						positions.clear();
						size = 0;
					}

					if (size > 2) {
						Pos pos2 = positions.get(size-1);
						Pos pos1 = positions.get(size-2);
						endX = -48.0;
						/*
						 * calcolo eq retta e punto intersezione con linea della porta
						 */
						m2 = (pos2.y-pos1.y)/(pos2.x-pos1.x);
						endY = (m2*(endX-pos1.x))+pos1.y;
						/*
						 * se la palla arriverà in porta e sta tirando, vadi al punto target
						 */
						if (endY >= UP_AREA_MARGIN && endY <= DOWN_AREA_MARGIN && Math.abs(pos2.x-pos1.x) > IS_SHOOTING_DIFFERENCE) {
							g.getAction().gotoPoint(new Pos(endX,endY));
							isGoing = true;
							System.out.println("sto andando a "+endX+" "+endY);
						}
					}
				}
			}
			else
				turnAndGoHome();
			
		}

	}
}

