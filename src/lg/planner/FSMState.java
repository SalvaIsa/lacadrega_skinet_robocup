package lg.planner;

public interface FSMState {
	
    void update(FSM fsm, Object object);

}
